package packageFilms;

/*
 * @author YANIS ALLOUCH
 * @author AHMED KACI 
 */
import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args) {
		Director d1 = new Director("1");
		d1.setDirectorName("Charlie");
		Film film1 = new Film("1","Film1",d1,"description Film1");
		Film film2 = new Film("2","Film2",d1,"description Film2");
		Film film3 = new Film("3","Film3",d1,"description Film3");
		
		d1.getFilms().add(film1);
		d1.getFilms().add(film2);
		d1.getFilms().add(film3);
		// affichage des information du réalisateur
		System.out.println("Réalistaeur : \n"+d1.toString());
		
		
		// affichage des films du 
		
		System.out.println("Liste de Films");
		List <Film> filmsDirector= d1.getFilms();
		for(Film f1: d1.getFilms()) {
			System.out.println("idFilm : "+f1.getId()+" | titre : "+f1.getTitle()+" | NomRéalisateur : "+f1.getDirector().getDirectorName()+" | Déscription :"+f1.getDescription());
		}
		
		
	}

}
