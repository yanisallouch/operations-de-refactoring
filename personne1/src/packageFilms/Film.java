package packageFilms;

/*
 * @author YANIS ALLOUCH
 * @author AHMED KACI 
 */
public class Film {
	
	private String id;
	private String title;
	private Director director;
	private String description;
	
	
	public Film() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Film(String id, String title, Director director, String description) {
		super();
		this.id = id;
		this.title = title;
		this.director = director;
		this.description = description;
	}

	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}
	public Director getDirector() {
		return director;
	}


	public void setDirector(Director director) {
		this.director = director;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Film [id = " + id + ", title = " + title + ", director = " + director.toString()
				+ ", description = " + description + "]";
	}

}
