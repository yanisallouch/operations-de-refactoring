package packageFilms;

/*
 * @author YANIS ALLOUCH
 * @author AHMED KACI 
 */
import java.util.ArrayList;
import java.util.List;

public class Director {
	private String id;
	List<Film> films;
	// d�placer cet attribut vers la classe du r�alisateur.
	private String directorName;
	
	public Director() {
		super();
		films = new ArrayList<Film>();
	}

	public Director(String id) {
		super();
		this.id = id;
		films = new ArrayList<Film>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	} 
	
	public List<Film> getFilms() {
		return films;
	}

	public void setFilms(List<Film> films) {
		this.films = films;
	}

	@Override
	public String toString() {
		return "Director [id = " + id + "]";
	}


	public String getDirectorName() {
		return directorName;
	}


	public void setDirectorName(String directorName) {
		this.directorName = directorName;
	}

	
}
