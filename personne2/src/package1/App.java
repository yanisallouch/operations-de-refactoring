package package1;

/*
 * @author YANIS ALLOUCH
 * @author AHMED KACI 
 */
public class App {

	public static void main(String[] args) {
		Student st1 = new Student();
		st1.setName("Charlie");
		st1.setStudentNumber("001");
		st1.setSpeciality("GL");

		System.out.println(st1.getName());
		System.out.println(st1.getStudentNumber());
		System.out.println(st1.getSpeciality());

	}

}
