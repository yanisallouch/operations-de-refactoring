package package1;

/*
 * @author YANIS ALLOUCH
 * @author AHMED KACI 
 */
public class Student extends Person {

	String speciality;
	private String studentNumber;


	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Student(String name, String studentNumber,String speciality) {
		super(name);
		this.speciality = speciality;
	}


	public String getSpeciality() {
		return speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}

	public String getStudentNumber() {
		return studentNumber;
	}

	public void setStudentNumber(String studentNumber) {
		this.studentNumber = studentNumber;
	}




}
