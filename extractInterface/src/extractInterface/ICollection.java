package extractInterface;

public interface ICollection {

	boolean add(Object o);

	boolean isEmpty();

}