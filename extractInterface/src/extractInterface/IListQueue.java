package extractInterface;

public interface IListQueue extends ICollection {

	Object peek();

	Object poll();

}